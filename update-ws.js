const mirrors = './mirrors/'
const icons = './icons/'
const fs = require('fs')

let websites = []
let deprecated = ["isMe", "removeBanners", "whereDoIWriteNavigation", "nextChapterUrl", "previousChapterUrl", "isImageInOneCol", "getMangaSelectFromPage"]
global.window = {}

global.registerAbstractImplementation = function(mirrorName) {
    websites.push({
        mirrorName: mirrorName,
        type: "abstract"
    })
}
global.registerMangaObject = function(object) {
    let mirrorName = object.mirrorName
    let website = {}
    if (!object.mirrorName) {
        console.error(mirrorName + " : mirrorName is required !")
    }
    website.mirrorName = mirrorName
    if (!object.domains) {
        console.error(mirrorName + " : domains is required !")
    }
    website.domains = object.domains
    if (!object.languages) {
        console.error(mirrorName + " : languages is required !")
    }
    website.home = object.home
    if (!object.home) {
        console.error(mirrorName + " : home is required !")
    }
    website.languages = object.languages
    if (!object.mirrorIcon) {
        console.error(mirrorName + " : mirrorIcon is required !")
    }
    website.mirrorIcon = base64_encode(icons + object.mirrorIcon)
    if (object.abstract !== undefined) {
        website.abstract = object.abstract
    }
    if (object.chapter_url) {
        if (typeof object.chapter_url === "string") website.chapter_url = object.chapter_url
        else if (object.chapter_url instanceof RegExp) website.chapter_url = object.chapter_url.toString()
    }
    websites.push(website)
    let deps = []
    for (let dep of deprecated) {
        if (object[dep]) deps.push(dep)
    }
    if (deps.length > 0) {
        console.log(mirrorName + " : functions " + deps.join(", ") + " have been deprecated")
    }
}

fs.readdir(mirrors, (err, files) => {
    let cur = 0
    files.forEach(async file => {
        require(mirrors + file)
        cur++
        await new Promise((resolve, reject) => {
            (function wait() {
                if (cur === websites.length) {
                    websites[websites.length - 1].jsFile = mirrors.substring(2) + file;
                    websites[websites.length - 1].id = cur;
                    resolve()
                } else setTimeout(wait, 10)
            })()
        })
    })
    writeWebsites()
})

/**
 * Write websites.json file
 */
function writeWebsites() {
    var json = JSON.stringify(websites)
    fs.writeFile('websites.json', json, () => {})
}

// function to encode file data to base64 encoded string
function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file)
    // convert binary data to base64 encoded string
    return "data:image/png;base64," + new Buffer(bitmap).toString('base64')
}