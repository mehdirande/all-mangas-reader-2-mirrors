if (typeof registerMangaObject === 'function') {
    registerMangaObject({
        mirrorName: "ChibiManga",
        mirrorIcon: "chibimanga.png",
        languages: "en",
        domains: ["www.cmreader.info"],
        home: "http://www.cmreader.info/",
        chapter_url: /^\/manga\/.*\/.*$/g,

        abstract: "MyMangaReaderCMS",
        abstract_options: {
            base_url: "http://www.cmreader.info"
        }
    })
}