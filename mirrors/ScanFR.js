if (typeof registerMangaObject === 'function') {
    registerMangaObject({
        mirrorName: "ScanFR",
        mirrorIcon: "scanfr.png",
        languages: "fr",
        domains: ["www.scan-fr.io", "scan-fr.io"],
        home: "https://www.scan-fr.io/",
        chapter_url: /^\/manga\/.*\/.*$/g,
        
        abstract: "MyMangaReaderCMS",
        abstract_options: {
            base_url: "https://www.scan-fr.io",
            chapters_element: "ul.chapterszz a[href*='/manga/']"
        }
    })
}